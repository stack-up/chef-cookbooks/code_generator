# Generator cookbook

## Set-up

Clone this repo and add to `knife.rb`:

```
chefdk.generator_cookbook '<PATH_TO_REPO>'
```

Replacing `<PATH_TO_REPO>` with the local path to wherever you've cloned this code_generator.

## Customizations:

### Cookbook generator:

 * Ubuntu 16.04 only
 * BSD 2-Clause License
 * Updates `README` and `metadata.rb`
 * adds metadata issues and source url (for chef supermarket)
